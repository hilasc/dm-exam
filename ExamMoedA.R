install.packages('tm')
library(tm)

install.packages('wordcloud')
library(wordcloud)

install.packages('e1071')
library(e1071)

install.packages('SDMTools')
library(SDMTools)

install.packages('mlr')
library(mlr)

data<-read.csv('examFile.csv')

###########Q1################

str(data)

data_corpus <- Corpus(VectorSource(data$text))
clean_corpus <- tm_map(data_corpus,removePunctuation)
clean_corpus
clean_corpus <- tm_map(clean_corpus,removeNumbers)
clean_corpus <-tm_map(clean_corpus,content_transformer(tolower))
clean_corpus <-tm_map(clean_corpus,removeWords,stopwords())
clean_corpus <-tm_map(clean_corpus,stripWhitespace)
clean_corpus[[1]][[1]]
dtm <- DocumentTermMatrix(clean_corpus)
dtm
inspect(dtm)
inspect(dtm[1:6,1:15])
dim(dtm)
frequent_dtm <- DocumentTermMatrix(clean_corpus,list(dictionary=findFreqTerms(dtm,4)))

dim(frequent_dtm)
inspect(frequent_dtm[1:6,1:17])

###########Q2################
pal <- brewer.pal(8,'Dark2')

wordcloud(clean_corpus[data$category == 'economy'], min.freq = 3, random.order = FALSE, colors = pal)
wordcloud(clean_corpus[data$category == 'Sport'], min.freq = 3, random.order = FALSE, colors = pal)

conv_yesno <- function(x){
  x<-ifelse(x>0,1,0)
  x<-factor(x,level=c(1,0),labels = c('yes','no'))
}

prep <- apply(frequent_dtm, MARGIN = 1:2, conv_yesno)
df_prep =as.data.frame(prep)
df_prep$category <-data$category
dim(df_prep)
df_prep[1:50, 16:18]
summary(df_prep)

###########Q3################

modelBase <- naiveBayes(df_prep[,-18],df_prep$category)

predictionBase <- predict(modelBase,df_prep[,-18])
predictionBase

con_10Base <-function(x){
  x<-ifelse(x=='economy',1,0)
}

pred01Base <- sapply(predictionBase, con_10Base)
actual01Base <- sapply(df_prep$category, con_10Base)

confusion.matrix(actual01Base,pred01Base)

###########Q4################


dataReg<-read.csv('examFile.csv')
split <- runif(nrow(dataReg))>0.3

DataRegTrain <- dataReg[split,]
DataRegTest <- dataReg[!split,]
str(dataReg)

logModel <- glm(text ~ category,
                data = DataRegTrain,
                family="binomial")

summary(logModel)


###########Q5################

predictedLog <- predict(logModel, DataRegTest[,-2], type="response")
predicted01Log <- ifelse(predictedLog>0.5,1,0)

conv_10Log <- function(x){
  x <- ifelse(x =='economy' ,1,0)
}

actual01Log <- sapply(DataRegTest$text,conv_10Log)
confusion.matrix(actual01Log, predicted01Log)

###########Q6################

dataClu<-read.csv('examFile.csv')
str(dataClu)

library(class)

set.seed(111111)
data_dum <- createDummyFeatures(dataClu, cols = names(dataClu$text))
cluters <- kmeans(data_dum,2)
cluters
